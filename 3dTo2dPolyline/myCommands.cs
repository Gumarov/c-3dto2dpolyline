﻿// (C) Copyright 2017 by  
//
using System;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using System.Collections.Generic;

namespace _3dTo2dPolyline
{
    public class MyCommands
    {
        [CommandMethod("3dTo2dPolyline")]
        static public void ReversePolylineDirection()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

           PromptEntityOptions peo = new PromptEntityOptions("\nEine 3dPolylinie wählen: ");
            peo.SetRejectMessage("\nist nicht 3dPolylinie.");
            peo.AddAllowedClass(typeof(Polyline3d), false);

            PromptEntityResult per = ed.GetEntity(peo);

            if (per.Status != PromptStatus.OK)
                return;

            Transaction tr = doc.TransactionManager.StartTransaction();
            using (tr)
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                Plane plane = new Plane(Point3d.Origin, Vector3d.ZAxis);
                Polyline3d myPoly = tr.GetObject(per.ObjectId, OpenMode.ForWrite) as Polyline3d;
                List <Point2d> Punkte2d = new List<Point2d>();
                foreach (ObjectId vId in myPoly)
                {
                    PolylineVertex3d vert = tr.GetObject(vId, OpenMode.ForRead) as PolylineVertex3d;
                    Punkte2d.Add(vert.Position.Convert2d(plane));
                }
                using (Polyline my2dPolyline = new Polyline())
                {                    ;
                    for(int i = 0; i < Punkte2d.Count; i++)
                    {
                        my2dPolyline.AddVertexAt(i, Punkte2d[i], 0, 0, 0);
                    }
                    my2dPolyline.Layer = myPoly.Layer;
                    my2dPolyline.Linetype = myPoly.Linetype;
                    my2dPolyline.LineWeight = myPoly.LineWeight;

                    myPoly.Erase();
                    ed.WriteMessage("\nFertig\n");

                    btr.AppendEntity(my2dPolyline);
                    tr.AddNewlyCreatedDBObject(my2dPolyline, true);
                }
                tr.Commit();
            }
        }
    }
}
